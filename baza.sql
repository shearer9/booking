-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.19 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table baza.admin
CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_korisnika` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_korisnika` (`id_korisnika`),
  CONSTRAINT `id_korisnika` FOREIGN KEY (`id_korisnika`) REFERENCES `korisnici` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table baza.admin: ~1 rows (approximately)
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` (`id`, `id_korisnika`) VALUES
	(3, 9);
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;

-- Dumping structure for table baza.adresa
CREATE TABLE IF NOT EXISTS `adresa` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ulica` varchar(50) NOT NULL DEFAULT '0',
  `grad` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Dumping data for table baza.adresa: ~3 rows (approximately)
/*!40000 ALTER TABLE `adresa` DISABLE KEYS */;
INSERT INTO `adresa` (`id`, `ulica`, `grad`) VALUES
	(9, 'Ante Star?evi?a', 'Mostar'),
	(10, 'Omladinska 13', 'Mostar'),
	(11, 'BB', 'Široki Brijeg');
/*!40000 ALTER TABLE `adresa` ENABLE KEYS */;

-- Dumping structure for table baza.apartmani
CREATE TABLE IF NOT EXISTS `apartmani` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `broj_sobe` int(11) NOT NULL,
  `klima` varchar(10) NOT NULL,
  `kvadrata` int(11) NOT NULL,
  `broj_kreveta` int(11) NOT NULL,
  `balkon` varchar(10) NOT NULL,
  `cijena` int(11) NOT NULL,
  `id_admina` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `broj_sobe` (`broj_sobe`),
  KEY `id_admina` (`id_admina`),
  CONSTRAINT `id_admina` FOREIGN KEY (`id_admina`) REFERENCES `admin` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;

-- Dumping data for table baza.apartmani: ~6 rows (approximately)
/*!40000 ALTER TABLE `apartmani` DISABLE KEYS */;
INSERT INTO `apartmani` (`id`, `broj_sobe`, `klima`, `kvadrata`, `broj_kreveta`, `balkon`, `cijena`, `id_admina`) VALUES
	(68, 5, 'Da', 42, 3, 'Ne', 70, 3),
	(69, 7, 'Da', 22, 2, 'Ne', 45, 3),
	(70, 31, 'Ne', 50, 3, 'Da', 80, 3),
	(71, 49, 'Da', 70, 4, 'Ne', 110, 3),
	(72, 51, 'Da', 51, 3, 'Da', 90, 3),
	(73, 23, 'Ne', 34, 2, 'Da', 65, 3);
/*!40000 ALTER TABLE `apartmani` ENABLE KEYS */;

-- Dumping structure for table baza.korisnici
CREATE TABLE IF NOT EXISTS `korisnici` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ime` varchar(50) NOT NULL,
  `prezime` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` blob NOT NULL,
  `id_adrese` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `adresaa` (`id_adrese`),
  CONSTRAINT `adresaa` FOREIGN KEY (`id_adrese`) REFERENCES `adresa` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Dumping data for table baza.korisnici: ~3 rows (approximately)
/*!40000 ALTER TABLE `korisnici` DISABLE KEYS */;
INSERT INTO `korisnici` (`id`, `ime`, `prezime`, `username`, `password`, `id_adrese`) VALUES
	(9, 'Ivan', 'Ostoji?', 'Ico', _binary 0x41579E3D233189C2E2C3AF45C10C96C5, 9),
	(10, 'test', 'test', 'test', _binary 0x41579E3D233189C2E2C3AF45C10C96C5, 10),
	(11, 'Filip', 'Filipovi?', 'Filip', _binary 0x41579E3D233189C2E2C3AF45C10C96C5, 11);
/*!40000 ALTER TABLE `korisnici` ENABLE KEYS */;

-- Dumping structure for table baza.rezervacije
CREATE TABLE IF NOT EXISTS `rezervacije` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_korisnika` int(10) unsigned NOT NULL,
  `id_apartmana` int(10) unsigned NOT NULL,
  `datum_od` date NOT NULL,
  `datum_do` date NOT NULL,
  `potvrdeno` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `rezervacija_apartman` (`id_apartmana`),
  KEY `rezervacija_korisnik` (`id_korisnika`),
  CONSTRAINT `rezervacija_apartman` FOREIGN KEY (`id_apartmana`) REFERENCES `apartmani` (`id`),
  CONSTRAINT `rezervacija_korisnik` FOREIGN KEY (`id_korisnika`) REFERENCES `korisnici` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- Dumping data for table baza.rezervacije: ~6 rows (approximately)
/*!40000 ALTER TABLE `rezervacije` DISABLE KEYS */;
INSERT INTO `rezervacije` (`id`, `id_korisnika`, `id_apartmana`, `datum_od`, `datum_do`, `potvrdeno`) VALUES
	(23, 10, 68, '2018-06-25', '2018-06-28', 0),
	(24, 10, 71, '2018-06-29', '2018-06-30', 1),
	(25, 10, 68, '2018-06-23', '2018-06-24', 0),
	(26, 11, 73, '2018-06-26', '2018-06-29', 0),
	(27, 11, 73, '2018-06-23', '2018-06-24', 0),
	(28, 11, 73, '2018-07-02', '2018-07-05', 1);
/*!40000 ALTER TABLE `rezervacije` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
