# Hotel reservations

Desktop application made in Swing for college assignment. Simple hotel reservation system 


## Installation

Create database and import included SQL 


```bash
cd src/cinema
open Connectionz.java

DriverManager.getConnection("jdbc:mysql://localhost:3306/{database_name}","{database_user}","{database_pwd");
```

## Usage
 
Admin is supposed to be a hotel's receptionist. He could add, update or delete rooms and he's responsible for confirming reservations.

User can reserve selected room for a specific period. He has insight into all his reservations.

Password: 123

## License
[MIT](https://choosealicense.com/licenses/mit/)